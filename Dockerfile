FROM roffe/kubectl

LABEL VERSION=1.2
LABEL DESCRIPTION="Deploy to kubernetes cluster easily"
LABEL maintainer="Hosco <dev@hosco.com>"

COPY ./src/setk8scluster.sh /usr/bin/setk8scluster
COPY ./src/deploy.sh /usr/bin/deploy
