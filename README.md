# ops-deploy-kubernetes

Allows someone to deploy an application using the [deploy script](/src/deploy.sh) or with `kubectl` commands.

## Usage

The following environment variables should be present when using this image to deploy kubernetes objects:

Development: 

 * KUBERNETES_CLUSTER_CERT_DEV
 * KUBERNETES_CLUSTER_URL_DEV
 * KUBERNETES_CLUSTER_TOKEN_DEV

Production:

 * KUBERNETES_CLUSTER_CERT_PRO
 * KUBERNETES_CLUSTER_URL_PRO
 * KUBERNETES_CLUSTER_TOKEN_PRO

### Deploy using the deploy script 

Run `deploy DEV namespace workload docker_image_path` and define `CI_JOB_ID` var env to force deployment of the same image.
For example:

 * `deploy DEV default facade-dev registry.hosco.com/ops-facade:latest`
 * `deploy PRO default facade-dev registry.hosco.com/ops-facade:1-0-0`

### Deploy using .yaml files and the kubectl command

Introduced in version [1.3.0](https://gitlab.com/hosco/ops-deploy-kubernetes/tags/1.3.0)

1. Choose the environment to deploy to: `setk8scluster dev`
2. Deploy your application: `kubectl apply -f the/k8s/yaml/directory`

Example configuration:

```yaml
deploy to dev:
  stage: deploy
  environment: 
    name: dev
  image: registry.gitlab.com/hosco/ops-deploy-kubernetes:1-3-0
  script:
    - setk8scluster dev
    - kubectl apply -f k8s/dev/
    - kubectl rollout status deployment/my-deployment
```
