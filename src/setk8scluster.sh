#!/usr/local/bin/bash

ENV=$1 # dev or pro

### check if all variables are set
if [ "$ENV" == "" ] ; then 
    echo ""
    echo "ERROR: please specify an environment [dev/pro]"
    echo ""
    exit 1
fi

### Configure depending on the environment
if [ "$ENV" == "dev" ]; then
    export KUBERNETES_CLUSTER_CERT=$KUBERNETES_CLUSTER_CERT_DEV
    export KUBERNETES_CLUSTER_URL=$KUBERNETES_CLUSTER_URL_DEV
    export KUBERNETES_CLUSTER_TOKEN=$KUBERNETES_CLUSTER_TOKEN_DEV

    echo "Environment set to DEVELOPMENT"

elif [ "$ENV" == "pro" ]; then
    export KUBERNETES_CLUSTER_CERT=$KUBERNETES_CLUSTER_CERT_PRO
    export KUBERNETES_CLUSTER_URL=$KUBERNETES_CLUSTER_URL_PRO
    export KUBERNETES_CLUSTER_TOKEN=$KUBERNETES_CLUSTER_TOKEN_PRO

    echo "Environment set to PRODUCTION"
else 
    echo "The given env is not valid. Please choose from [dev/pro]"
    exit 1
fi

printenv KUBERNETES_CLUSTER_CERT > ./ca.crt
kubectl config set-cluster main-cluster --server=${KUBERNETES_CLUSTER_URL} --certificate-authority=./ca.crt
kubectl config set-credentials main-user --token=${KUBERNETES_CLUSTER_TOKEN}
kubectl config set-context main-context --cluster=main-cluster --user=main-user
kubectl config use-context main-context
