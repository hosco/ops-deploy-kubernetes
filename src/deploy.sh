#!/usr/local/bin/bash

ENV=$1 # => DEV or PRO
NAMESPACE=$2 # => Namespace in kubernetes
WORKLOAD=$3 # => Workload in kubernetes
CONTAINER_IMAGE=$4

LOWERCASE_ENV=`echo $ENV | tr '[:upper:]' '[:lower:]'`
setk8scluster $LOWERCASE_ENV

if [ $? -ne 0 ]; then
    echo "Unable to set cluster"
    exit 1
fi

### ###########################
### Launch the deployment
kubectl --namespace=$NAMESPACE patch deployment $WORKLOAD --type=strategic -p \
    '{ "spec": { "template": { "spec": { "containers": [{ "name": "'${WORKLOAD}'", "image": "'${CONTAINER_IMAGE}'", "env": [{ "name":"CI_JOB_ID", "value":"'${CI_JOB_ID}'" }] }] } } } }'
patched=$?

if [ $patched != 0 ]; then
    echo "Can't patch container"
    exit 1
fi
### End Launch the deployment

### ###########################
### Wait the deployment
max=15 # Max 15min to do a new deployment
counter=1
status=1

while [ $status != 0 -a $counter -lt $max ]
do
    # Need to check more than once because the timeout of rollout is 1min
    echo "Check the new status"
    kubectl rollout --namespace=$NAMESPACE status deployments/$WORKLOAD
    status=$?
    counter=$(( $counter + 1 ))
done

if [ $status != 0 ]; then
    echo "Deployment failed"
    exit 1
fi
### End Wait the deployment

echo "Deployment done"
exit 0
